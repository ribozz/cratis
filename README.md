Cratis
==================

Crutis is a full-stack application platform, that contains everything
needed for mdern web-application development:

- Django based
- Convinient modular configuration system (Cratis Features + django-configurations)
- modern developer friendly Django-cms
- Central picture storage with auto-resize right in your template code (Filer)
- Tight integration of picture storage with django-cms (django-filer)
- Filer widgets for cms (picture, file, folder)
- Frontend themes (theme = assets + templates) with inheritace support
- full localization support (templates, strings in code, models, urls, cms pages)
- django-dbug toolbar integrated
- Nice admin theme "suit" integrated. Make sure to buy a licence if you are not open source: http://djangosuit.com/
- Admin menu structure control
- Django-cms onsite editing
- sorl database migrations integrated
- high-performance cherry-py web-server integrated
- Zero-configuration and zero-code startup


How-to:

    $ pip install django-cratis

Simplest ever startup:

    cratis runserver

Cratis command is basically manage.py script of django, but installed into bin/ folder + some code to make
it universal.

Add some functionality, Create settings.py

    from cratis.features import AdminArea, AdminThemeSuit, Debug, Cms
    from cratis.settings import Dev as CratisDev


    class Dev(CratisDev):
        FEATURES = (
            AdminThemeSuit(),
            AdminArea(),
            Debug()
        )


Run in production mode. Create supervisor.conf:

    [program:app]
    command=/home/app/env/bin/cratis runwsgiserver
    directory=/home/app/yourappdir
    
    
Support
------------------
    
For support/questions mail to: cratis-project@googlegroups.com



