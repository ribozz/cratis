import os


def ecommerce_theme():
    return os.path.dirname(__file__) + '/themes/ecommerce'
